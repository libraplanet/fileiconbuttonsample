﻿using System;
using System.Runtime.InteropServices;

namespace Win32.Api
{
    /// <summary>
    /// Def
    /// 参考:http://iisoft.jp/tips/2013022302_explorerContextMenu.html
    /// </summary>
    public static class Def
    {
#region [enum]
        [Flags()]
        public enum ContextMenuFlags : uint
        {
            Normal = 0x00000000,
            DefaultOnly = 0x00000001,
            VerbsOnly = 0x00000002,
            Explore = 0x00000004,
            NoVerbs = 0x00000008,
            CanRename = 0x00000010,
            NoDefault = 0x00000020,
            IncludeStatic = 0x00000040,
            Reserved = 0xffff0000      // View specific
        }
        [Flags()]
        public enum QueryContextMenuFlags
        {
            CMF_NORMAL = 0,
            CMF_DEFAULTONLY = 1,
            CMF_VERBSONLY = 2,
            CMF_EXPLORE = 4,
            CMF_NOVERBS = 8,
            CMF_CANRENAME = 0x10,
            CMF_NODEFAULT = 0x20,
            CMF_INCLUDESTATIC = 0x40,
            CMF_EXTENDEDVERBS = 0x100,
            CMF_RESERVED = unchecked((int)0xFFFF0000)
        }
        [Flags]
        public enum SHGNO : uint
        {
            SHGDN_NORMAL = 0x0000,  // default (display purpose)
            SHGDN_INFOLDER = 0x0001,  // displayed under a folder (relative)
            SHGDN_FOREDITING = 0x1000,  // for in-place editing
            SHGDN_FORADDRESSBAR = 0x4000,  // UI friendly parsing name (remove ugly stuff)
            SHGDN_FORPARSING = 0x8000,  // parsing name for ParseDisplayName()
        }
        [Flags]
        public enum SHCONTF : uint
        {
            SHCONTF_FOLDERS = 0x0020,   // only want folders enumerated (SFGAO_FOLDER)
            SHCONTF_NONFOLDERS = 0x0040,   // include non folders
            SHCONTF_INCLUDEHIDDEN = 0x0080,   // show items normally hidden
            SHCONTF_INIT_ON_FIRST_NEXT = 0x0100,   // allow EnumObject() to return before validating enum
            SHCONTF_NETPRINTERSRCH = 0x0200,   // hint that client is looking for printers
            SHCONTF_SHAREABLE = 0x0400,   // hint that client is looking sharable resources (remote shares)
            SHCONTF_STORAGE = 0x0800,   // include all items with accessible storage and their ancestors
        }
        [Flags]
        public enum SFGAOF : uint
        {
            SFGAO_CANCOPY = 0x1,                // Objects can be copied    (DROPEFFECT_COPY)
            SFGAO_CANMOVE = 0x2,                // Objects can be moved     (DROPEFFECT_MOVE)
            SFGAO_CANLINK = 0x4,                // Objects can be linked    (DROPEFFECT_LINK)
            SFGAO_STORAGE = 0x00000008,         // supports BindToObject(IID_IStorage)
            SFGAO_CANRENAME = 0x00000010,         // Objects can be renamed
            SFGAO_CANDELETE = 0x00000020,         // Objects can be deleted
            SFGAO_HASPROPSHEET = 0x00000040,         // Objects have property sheets
            SFGAO_DROPTARGET = 0x00000100,         // Objects are drop target
            SFGAO_CAPABILITYMASK = 0x00000177,
            SFGAO_ENCRYPTED = 0x00002000,         // object is encrypted (use alt color)
            SFGAO_ISSLOW = 0x00004000,         // 'slow' object
            SFGAO_GHOSTED = 0x00008000,         // ghosted icon
            SFGAO_LINK = 0x00010000,         // Shortcut (link)
            SFGAO_SHARE = 0x00020000,         // shared
            SFGAO_READONLY = 0x00040000,         // read-only
            SFGAO_HIDDEN = 0x00080000,         // hidden object
            SFGAO_DISPLAYATTRMASK = 0x000FC000,
            SFGAO_FILESYSANCESTOR = 0x10000000,         // may contain children with SFGAO_FILESYSTEM
            SFGAO_FOLDER = 0x20000000,         // support BindToObject(IID_IShellFolder)
            SFGAO_FILESYSTEM = 0x40000000,         // is a win32 file system object (file/folder/root)
            SFGAO_HASSUBFOLDER = 0x80000000,         // may contain children with SFGAO_FOLDER
            SFGAO_CONTENTSMASK = 0x80000000,
            SFGAO_VALIDATE = 0x01000000,         // invalidate cached information
            SFGAO_REMOVABLE = 0x02000000,         // is this removeable media?
            SFGAO_COMPRESSED = 0x04000000,         // Object is compressed (use alt color)
            SFGAO_BROWSABLE = 0x08000000,         // supports IShellFolder, but only implements CreateViewObject() (non-folder view)
            SFGAO_NONENUMERATED = 0x00100000,         // is a non-enumerated object
            SFGAO_NEWCONTENT = 0x00200000,         // should show bold in explorer tree
            SFGAO_CANMONIKER = 0x00400000,         // defunct
            SFGAO_HASSTORAGE = 0x00400000,         // defunct
            SFGAO_STREAM = 0x00400000,         // supports BindToObject(IID_IStream)
            SFGAO_STORAGEANCESTOR = 0x00800000,         // may contain children with SFGAO_STORAGE or SFGAO_STREAM
            SFGAO_STORAGECAPMASK = 0x70C50008,         // for determining storage capabilities, ie for open/save semantics
        }
        [Flags]
        public enum TPM : uint
        {
            TPM_LEFTBUTTON = 0x0000,
            TPM_RIGHTBUTTON = 0x0002,
            TPM_LEFTALIGN = 0x0000,
            TPM_CENTERALIGN = 0x0004,
            TPM_RIGHTALIGN = 0x0008,
            TPM_TOPALIGN = 0x0000,
            TPM_VCENTERALIGN = 0x0010,
            TPM_BOTTOMALIGN = 0x0020,
            TPM_HORIZONTAL = 0x0000,
            TPM_VERTICAL = 0x0040,
            TPM_NONOTIFY = 0x0080,
            TPM_RETURNCMD = 0x0100,
            TPM_RECURSE = 0x0001,
            TPM_HORPOSANIMATION = 0x0400,
            TPM_HORNEGANIMATION = 0x0800,
            TPM_VERPOSANIMATION = 0x1000,
            TPM_VERNEGANIMATION = 0x2000,
            TPM_NOANIMATION = 0x4000,
            TPM_LAYOUTRTL = 0x8000
        }
        [Flags]
        public enum CMIC : uint
        {
            CMIC_MASK_HOTKEY = 0x00000020,
            CMIC_MASK_ICON = 0x00000010,
            CMIC_MASK_FLAG_NO_UI = 0x00000400,
            CMIC_MASK_UNICODE = 0x00004000,
            CMIC_MASK_NO_CONSOLE = 0x00008000,
            CMIC_MASK_ASYNCOK = 0x00100000,
            CMIC_MASK_NOZONECHECKS = 0x00800000,
            CMIC_MASK_SHIFT_DOWN = 0x10000000,
            CMIC_MASK_CONTROL_DOWN = 0x40000000,
            CMIC_MASK_FLAG_LOG_USAGE = 0x04000000,
            CMIC_MASK_PTINVOKE = 0x20000000
        }
        [Flags]
        public enum GetCommandStringFlags : uint
        {
            GCS_VERBA = 0,
            GCS_HELPTEXTA = 1,
            GCS_VALIDATEA = 2,
            GCS_VERBW = 4,
            GCS_HELPTEXTW = 5,
            GCS_VALIDATEW = 6
        }
        [Flags]
        public enum ShowWindowCommand : uint
        {
            /// <summary>
            ///        Hides the window and activates another window.
            /// </summary>
            SW_HIDE = 0,

            /// <summary>
            ///        Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.
            /// </summary>
            SW_SHOWNORMAL = 1,

            /// <summary>
            ///        Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.
            /// </summary>
            SW_NORMAL = 1,

            /// <summary>
            ///        Activates the window and displays it as a minimized window.
            /// </summary>
            SW_SHOWMINIMIZED = 2,

            /// <summary>
            ///        Activates the window and displays it as a maximized window.
            /// </summary>
            SW_SHOWMAXIMIZED = 3,

            /// <summary>
            ///        Maximizes the specified window.
            /// </summary>
            SW_MAXIMIZE = 3,

            /// <summary>
            ///        Displays a window in its most recent size and position. This value is similar to <see cref="ShowWindowCommands.SW_SHOWNORMAL"/>, except the window is not activated.
            /// </summary>
            SW_SHOWNOACTIVATE = 4,

            /// <summary>
            ///        Activates the window and displays it in its current size and position.
            /// </summary>
            SW_SHOW = 5,

            /// <summary>
            ///        Minimizes the specified window and activates the next top-level window in the z-order.
            /// </summary>
            SW_MINIMIZE = 6,

            /// <summary>
            ///        Displays the window as a minimized window. This value is similar to <see cref="ShowWindowCommands.SW_SHOWMINIMIZED"/>, except the window is not activated.
            /// </summary>
            SW_SHOWMINNOACTIVE = 7,

            /// <summary>
            ///        Displays the window in its current size and position. This value is similar to <see cref="ShowWindowCommands.SW_SHOW"/>, except the window is not activated.
            /// </summary>
            SW_SHOWNA = 8,

            /// <summary>
            ///        Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when restoring a minimized window.
            /// </summary>
            SW_RESTORE = 9
        }
#endregion
#region [struct]

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct CMINVOKECOMMANDINFOEX
        {
            public int cbSize;
            public CMIC fMask;
            public IntPtr hwnd;
            public IntPtr lpVerb;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpDirectory;
            public ShowWindowCommand nShow;
            public int dwHotKey;
            public IntPtr hIcon;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpTitle;
            public IntPtr lpVerbW;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpParametersW;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpDirectoryW;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpTitleW;
            public POINT ptInvoke;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }

            public POINT(System.Drawing.Point pt) : this(pt.X, pt.Y) { }

            public static implicit operator System.Drawing.Point(POINT p)
            {
                return new System.Drawing.Point(p.X, p.Y);
            }

            public static implicit operator POINT(System.Drawing.Point p)
            {
                return new POINT(p.X, p.Y);
            }
        }
#endregion
    }
}
