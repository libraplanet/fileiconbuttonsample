﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Win32.Api
{
    public static class ShFolder
    {
        [DllImport("shell32.dll")]
        static extern int SHGetDesktopFolder(ref IShellFolder ppshf);
    }
}
