﻿using System;
using System.Runtime.InteropServices;

namespace Win32.Api
{
    /// <summary>
    /// IContextMenu
    /// 参考:http://iisoft.jp/tips/2013022302_explorerContextMenu.html
    /// </summary>
    [ComImport()]
    [Guid("000214E4-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IContextMenu
    {
        [PreserveSig()]
        int QueryContextMenu(IntPtr hMenu, int indexMenu, int idCmdFirst, int idCmdLast, Def.QueryContextMenuFlags uFlags);

        [PreserveSig()]
        void InvokeCommand(ref Def.CMINVOKECOMMANDINFOEX lpici);

        [PreserveSig()]
        void GetCommandString(
            int idCmd,
            Def.GetCommandStringFlags uType,
            int pwReserved,
            [MarshalAs(UnmanagedType.LPArray)]
        byte[] pszName,
            int cchMax
            );
    }
}
