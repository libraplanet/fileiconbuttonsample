﻿using System;
using System.Runtime.InteropServices;

namespace Win32.Api
{
    /// <summary>
    /// IShellFolder
    /// 参考:http://iisoft.jp/tips/2013022302_explorerContextMenu.html
    /// </summary>
    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("000214E6-0000-0000-C000-000000000046")]
    public interface IShellFolder
    {
        [PreserveSig()]
        uint ParseDisplayName(
            IntPtr hwnd,
            IntPtr pbc,
            [In(), MarshalAs(UnmanagedType.LPWStr)]
        string pszDisplayName,
            out uint pchEaten,
            out IntPtr ppidl,
            ref uint pdwAttributes
            );

        [PreserveSig()]
        uint EnumObjects(
            IntPtr hwnd,
            Def.SHCONTF grfFlags,
            out IEnumIDList ppenumIDList
            );

        [PreserveSig()]
        uint BindToObject(
            IntPtr pidl,
            IntPtr pbc,
            [In()]
        ref Guid riid,
            // [MarshalAs(UnmanagedType.Interface)]
            out IntPtr ppv
            );

        [PreserveSig()]
        uint BindToStorage(
            IntPtr pidl,
            IntPtr pbc,
            [In()]
        ref Guid riid,
            [MarshalAs(UnmanagedType.Interface)]
        out object ppv
            );

        [PreserveSig()]
        int CompareIDs(
            int lParam,
            IntPtr pidl1,
            IntPtr pidl2
            );

        [PreserveSig()]
        uint CreateViewObject(
            IntPtr hwndOwner,
            [In()]
        ref Guid riid,
            [MarshalAs(UnmanagedType.Interface)]
        out object ppv
            );

        [PreserveSig()]
        uint GetAttributesOf(
            int cidl,
            [In(), MarshalAs(UnmanagedType.LPArray)] IntPtr[]
            apidl,
            ref Def.SFGAOF rgfInOut
            );

        [PreserveSig()]
        uint GetUIObjectOf(
            IntPtr hwndOwner,
            uint cidl,
            [In(), MarshalAs(UnmanagedType.LPArray)] IntPtr[]
            apidl,
            [In()]
        ref Guid riid,
            IntPtr rgfReserved,
            out IntPtr ppv
            );

        [PreserveSig()]
        uint GetDisplayNameOf(
            IntPtr pidl,
            Def.SHGNO uFlags,
            IntPtr pName
            );

        [PreserveSig()]
        uint SetNameOf(
            IntPtr hwnd,
            IntPtr pidl,
            [In(), MarshalAs(UnmanagedType.LPWStr)]
        string pszName,
            Def.SHGNO uFlags,
            out IntPtr ppidlOut
            );
    }
}
