﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32.Api
{
    /// <summary>
    /// IContextMenu3
    /// 参考:http://iisoft.jp/tips/2013022302_explorerContextMenu.html
    /// </summary>
    [ComImport, Guid("bcfce0a0-ec17-11d0-8d10-00a0c90f2719")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IContextMenu3
    {
        [PreserveSig()]
        int QueryContextMenu(
            IntPtr hmenu,
            uint iMenu,
            uint idCmdFirst,
            uint idCmdLast,
            Def.ContextMenuFlags uFlags);

        [PreserveSig()]
        int InvokeCommand(
            ref Def.CMINVOKECOMMANDINFOEX info);

        [PreserveSig()]
        int GetCommandString(
            uint idcmd,
            Def.GetCommandStringFlags uflags,
            uint reserved,
            [MarshalAs(UnmanagedType.LPWStr)]
        StringBuilder pszName,
            int cch);

        [PreserveSig()]
        int HandleMenuMsg(
            uint uMsg,
            IntPtr wParam,
            IntPtr lParam);

        [PreserveSig()]
        int HandleMenuMsg2(
            uint uMsg,
            IntPtr wParam,
            IntPtr lParam,
            IntPtr plResult);
    }
}
