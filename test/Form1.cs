﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using Win32.Api;

namespace Test
{
    public partial class Form1 : Form
    {
        delegate void Func();

        public Form1()
        {
            InitializeComponent();
            SuspendLayout();
            checkBox1.Checked = true;
            checkBox2.Checked = true;
            fileButton1.Message = this.Message;
            fileButton2.Message = this.Message;
            fileButton3.Message = this.Message;
            fileButton4.Message = this.Message;
            try
            {
                string path = Path.GetFullPath("sample.xls");
                openFileDialog1.FileName = path;
                textBox1.Text = path;
            }
            catch
            {
            }
            ResumeLayout(false);
        }

        void Message(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), s));
            using (StringReader r = new StringReader(textBox2.Text))
            {
                for (int i = 1; i < 100; i++)
                {
                    string line = r.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    else
                    {
                        sb.AppendLine(line);
                    }
                }
            }
            textBox2.Text = sb.ToString();
        }

        private void Exec(Func f)
        {
            try
            {
                f();
            }
            catch (Exception e)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("appear exception!");
                sb.Append(e.ToString());
                Message(sb.ToString());
            }
        }

        private void Exec(Func f, string caption, string successMessage, string failedMessage, bool isOutputException)
        {
            try
            {
                f();
                Message(string.Format("{0}{1}", caption, successMessage));
            }
            catch (Exception e)
            {
                if (isOutputException)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("appear exception!");
                    sb.Append(e.ToString());
                    Message(sb.ToString());
                }
                Message(string.Format("{0}{1}", caption, failedMessage));
            }
        }


        private void buttonBrows_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = "";
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string filePath = textBox1.Text;
            if (string.IsNullOrEmpty(filePath))
            {
                pictureBox1.Image = null;
                pictureBox2.Image = null;
                imageList1.Images.Clear();
                listView1.Items.Clear();
                imageList2.Images.Clear();
                listView2.Items.Clear();
                fileButton1.SetFilePath(null, null);
                fileButton2.SetFilePath(null, null);
                fileButton3.SetFilePath(null, null);
                fileButton4.SetFilePath(null, null);
                webBrowser1.Navigate((string)null);
            }
            else if (File.Exists(filePath))
            {

                Message(string.Format("\"{0}\"は存在します。", filePath));
                Message(string.Format("\"{0}\"より情報のセット開始。", filePath));
                // (P/Invoke)
                {
                    Icon iconL = null;
                    Icon iconS = null;
                    Exec(delegate()
                    {
                        iconL = ShFile.GetIcon(filePath, ShFile.SHGFI_LARGEICON);
                        Message("SHGetFileInfoにてSHGFI_LARGEICONアイコン取得");
                    });
                    Exec(delegate()
                    {
                        iconS = ShFile.GetIcon(filePath, ShFile.SHGFI_SMALLICON);
                        Message("SHGetFileInfoにてSHGFI_SMALLICONアイコン取得");
                    });
                    Exec(delegate()
                    {
                        pictureBox1.Image = iconL.ToBitmap();
                        Message("アイコンをpictureBox1にセット");
                    });
                    Exec(delegate()
                    {
                        imageList1.Images.Add(iconL);
                        Message("アイコンをimageList1にセット");
                        listView1.Items.Add(Path.GetFileName(filePath), 0);
                        Message("ファイル情報をlistView1にセット");
                    });
                    Exec(delegate()
                    {
                        fileButton1.SetFilePath(filePath, iconL);
                        Message("ファイル情報をfileButton1にセット");
                    });
                    Exec(delegate()
                    {
                        fileButton3.SetFilePath(filePath, null);
                        Message("ファイル情報をアイコンなしでfileButton3にセット");
                    });
                }
                //Icon.ExtractAssociatedIcon(Managed)
                {
                    Icon icon = null;
                    Exec(delegate()
                    {

                        icon = Icon.ExtractAssociatedIcon(filePath);
                        Message("Icon.ExtractAssociatedIconにてアイコン取得");

                        Exec(delegate()
                        {
                            pictureBox2.Image = icon.ToBitmap();
                            Message("アイコンをpictureBox2にセット");
                        });
                        Exec(delegate()
                        {
                            imageList2.Images.Add(icon);
                            Message("アイコンをimageList2にセット");
                            listView2.Items.Add(Path.GetFileName(filePath), 0);
                            Message("ファイル情報をlistView2にセット");
                        });
                        Exec(delegate()
                        {
                            fileButton2.SetFilePath(filePath, icon);
                            Message("ファイル情報をfileButton2にセット");
                        });
                    });
                    Exec(delegate()
                    {
                        fileButton4.SetFilePath(filePath, null);
                        Message("ファイル情報をアイコンなしでfileButton4にセット");
                    });
                    Message(string.Format("\"{0}\"より情報のセット完了。", filePath));
                }
                //WebBrowser(Managed)
                {
                    Exec(delegate()
                    {
                        webBrowser1.Navigate(Path.GetDirectoryName(filePath));
                    });
                }
                {
                    Exec(delegate()
                    {
                        Uri uri = new Uri(filePath);
                        string format = "[Icon.ExtractAssociatedIcon(\"{0}\")] {1}:";
                        Exec(delegate()
                        {
                            Icon.ExtractAssociatedIcon(uri.OriginalString);
                        }, string.Format(format, "uri.OriginalString()", uri.OriginalString), "成功", "失敗", true);
                        Exec(delegate()
                        {
                            Icon.ExtractAssociatedIcon(uri.LocalPath);
                        }, string.Format(format, "uri.LocalPath()", uri.LocalPath), "成功", "失敗", true);
                        Exec(delegate()
                        {
                            Icon.ExtractAssociatedIcon(uri.AbsolutePath);
                        }, string.Format(format, "uri.AbsolutePath()", uri.AbsolutePath), "成功", "失敗", true);
                        Exec(delegate()
                        {
                            Icon.ExtractAssociatedIcon(uri.AbsoluteUri);
                        }, string.Format(format, "uri.AbsoluteUri()", uri.AbsoluteUri), "成功", "失敗", true);
                    });
                }
            }
            else
            {
                Message(string.Format("\"{0}\"は存在しません。", filePath));
            }
        }

        private void OpenFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                Exec(delegate()
                {
                    try
                    {
                        Message(string.Format("\"{0}\"を開きます。", filePath));
                        Process.Start(filePath);
                        Message(string.Format("\"{0}\"を開きました。", filePath));
                    }
                    catch (Win32Exception ex)
                    {
                        if (ex.NativeErrorCode == 1155)
                        {
                            Message("ファイルが関連づいていません。");
                        }
                        else
                        {
                            Message("Openに失敗しました。");
                        }
                    }
                    catch
                    {
                        Message("Openに失敗しました。");
                    }
                });
            }
        }

        private void DragFile(Control c, string filePath)
        {
            if (File.Exists(filePath))
            {
                DataObject dataObject = new DataObject(DataFormats.FileDrop, new string[] { filePath });
                DragDropEffects effect = DragDropEffects.Copy;
                c.DoDragDrop(dataObject, effect);
            }
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }

        private void openOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }
        private void fileButton1_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }
        private void fileButton2_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }
        private void fileButton3_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }
        private void fileButton4_Click(object sender, EventArgs e)
        {
            OpenFile(textBox1.Text);
        }

        private void button1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            Message("button1_QueryContinueDrag (" + e.Action + ")");
        }
        private void button2_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            Message("button1_QueryContinueDrag (" + e.Action + ")");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            fileButton1.IsGhost = checkBox1.Checked;
            fileButton3.IsGhost = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            fileButton2.IsGhost = checkBox2.Checked;
            fileButton4.IsGhost = checkBox2.Checked;
        }
    }
}
