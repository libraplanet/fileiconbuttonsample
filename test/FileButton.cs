﻿using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace Test
{
    public partial class FileButton : Button
    {
        delegate void Func();

        public delegate void DelegateMessage(string s);

        public DelegateMessage Message { get; set; }

        public bool IsGhost { get; set; }

        private class GhostForm : Form
        {
            /// <summary>
            /// クリック通過
            /// </summary>
            private static readonly int WS_EX_TRANSPARENT = 0x00000020;
            /// <summary>
            /// 非アクティブ (Task Switchに非表示)
            /// </summary>
            private static readonly int WS_EX_NOACTIVATE = 0x08000000;

            PictureBox pictureBox = new PictureBox();

            public GhostForm()
            {
                SuspendLayout();
                //form setting
                {
                    FormBorderStyle = FormBorderStyle.None;
                    StartPosition = FormStartPosition.Manual;
                    TopMost = true;
                    ShowInTaskbar = false;
                    MinimumSize = new Size(48, 48);
                    Opacity = 0.5f;
                }

                //picture box setting
                {
                    pictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                    pictureBox.BackColor = Color.White;
                    pictureBox.Dock = DockStyle.Fill;
                    pictureBox.Paint += new PaintEventHandler(pictureBox_Paint);
                }

                this.Controls.Add(pictureBox);

                ResumeLayout(false);
            }

            protected override CreateParams CreateParams
            {
                get
                {
                    CreateParams cp = base.CreateParams;
                    cp.ExStyle |= WS_EX_TRANSPARENT;
                    cp.ExStyle |= WS_EX_NOACTIVATE;
                    return cp;
                }
            }

            public void SetIcon(Icon icon)
            {
                if (icon == null)
                {
                    SetImage(null);
                }
                else
                {
                    SetImage(icon.ToBitmap());
                }
            }

            public void SetImage(Image img)
            {
                if (img == null)
                {
                    pictureBox.Image = null;
                }
                else
                {
                    pictureBox.Image = img;
                }
            }

            public void SizeFit()
            {
                Image img = this.pictureBox.Image;
                if (img != null)
                {
                    Size s = img.Size;
                    Size = new Size(s.Width + 25, s.Height + 25);
                }
                else
                {
                    Size = new Size(0, 0);
                }
            }

            void pictureBox_Paint(object sender, PaintEventArgs e)
            {
                e.Graphics.DrawRectangle(new Pen(new SolidBrush(SystemColors.Highlight), 1), 0, 0, Width - 1, Height - 1);
            }

            public void MoveToMousePosition()
            {
                Point p = Cursor.Position;
                p.X -= (Size.Width * 4 / 5);
                p.Y -= (Size.Height * 4 / 5);
                Location = p;
            }
        }

        private string filePath;
        Func dragStartFunc;

        GhostForm ghostForm = new GhostForm();

        private void Trace(string s)
        {
            Debug.WriteLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), s));
            if (this.Message != null)
            {
                this.Message(s);
            }
        }

        public FileButton()
        {
            InitializeComponent();
            this.SuspendLayout();
            this.ImageList = imageList;
            this.AllowDrop = true;
            this.Size = new Size(48, 48);
            ghostForm.SetIcon(null);
            this.ResumeLayout(false);
        }

        public override string Text
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public void SetFilePath(string path, Icon icon)
        {
            ImageIndex = -1;
            imageList.Images.Clear();
            ghostForm.SetIcon(null);
            if (icon != null)
            {
                imageList.Images.Add(icon);
                ImageIndex = 0;
                ghostForm.SetIcon(icon);
            }
            filePath = path;
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            base.OnMouseDown(mevent);

            if (mevent.Button == MouseButtons.Left)
            {
                string filePath = this.filePath;
                bool isGhost = this.IsGhost;
                Func f = delegate() { StartDragDrop(filePath, isGhost); };
                Trace("FileButton.OnMouseDown() regist dragStartFunc.");
                dragStartFunc = f;
            }
        }

        protected override void OnMouseMove(MouseEventArgs mevent)
        {
            base.OnMouseMove(mevent);

            Func f = dragStartFunc;
            if (f != null)
            {
                Trace("FileButton.OnMouseMove() call dragStartFunc.");
                dragStartFunc = null;
                f();
            }
        }

        private void StartDragDrop(string filePath, bool isGhost)
        {
            if (File.Exists(filePath))
            {
                Trace("FileButton.OnMouseDown() setting DoDragDrop.");
                DataObject dataObject = new DataObject(DataFormats.FileDrop, new string[] { filePath });
                DragDropEffects effect = DragDropEffects.Copy;
#if false
                ghostForm.MoveToMousePosition();
                if (isGhost)
                {
                    Form f = Form.ActiveForm;
                    ghostForm.Show(this);
                    ghostForm.SizeFit();
                    ghostForm.Invalidate();
                    f.Focus();
                }
                Trace("FileButton.OnMouseDown() start DoDragDrop.");
                DoDragDrop(dataObject, effect);
                Trace("FileButton.OnMouseDown() end DoDragDrop.");
#else
                QueryContinueDragEventHandler queryEvent = null;
                try
                {
                    using (GhostForm ghostForm = new GhostForm())
                    {
                        Form f = Form.ActiveForm;
                        Image icoImg;
                        queryEvent = new QueryContinueDragEventHandler(delegate(object sender, QueryContinueDragEventArgs e)
                        {
                            if (e.Action == DragAction.Continue)
                            {
                                Trace("FileButton.QueryContinueDrag() drag move. ghostForm.Visible(" + ghostForm.Visible + ")");
                                ghostForm.MoveToMousePosition();
                            }
                            else
                            {
                                Trace("FileButton.QueryContinueDrag() drag end.");
                                //ghostForm.Close();
                            }
                        });
                        if (imageList.Images.Count > 0)
                        {
                            icoImg = imageList.Images[0];
                        }
                        else
                        {
                            icoImg = null;
                        }
                        this.QueryContinueDrag += queryEvent;
                        ghostForm.SetImage(icoImg);
                        ghostForm.Show(this);
                        ghostForm.SizeFit();
                        ghostForm.Invalidate();
                        f.Focus();
                        Trace("FileButton.OnMouseDown() start DoDragDrop.");
                        DoDragDrop(dataObject, effect);
                    }
                }
                finally
                {
                    if (queryEvent != null)
                    {
                        this.QueryContinueDrag -= queryEvent;
                    }
                    Trace("FileButton.OnMouseDown() end DoDragDrop.");
                }
#endif
            }
        }

        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs qcdevent)
        {
            base.OnQueryContinueDrag(qcdevent);

            if (ghostForm.Visible)
            {
                if (qcdevent.Action == DragAction.Continue)
                {
                    Trace("FileButton.OnQueryContinueDrag() drag move. ghostForm.Visible(" + ghostForm.Visible + ")");
                    ghostForm.MoveToMousePosition();
                }
                else
                {
                    Trace("FileButton.OnQueryContinueDrag() drag end.");
                    ghostForm.Hide();
                }
            }
        }
    }
}
